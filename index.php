<?php

class Basket {
    private $basket = [];

    private static $instance;

    private function __construct()
    {
        if (!empty($_COOKIE['Basket'])) {
            $basketFromCookie = json_decode($_COOKIE['Basket'], true);
            $this->basket = $basketFromCookie;
        }
    }

    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new Basket();
        }
        return self::$instance;
    }

    public function setProduct($id, $name, $price) {

        if (!empty($this->basket[$id])) {
            $this->basket[$id]['count']++;
        } else {
            $this->basket[$id]['id'] = $id;
            $this->basket[$id]['name'] = $name;
            $this->basket[$id]['price'] = $price;
            $this->basket[$id]['count'] = 1;
        }

        $this->setCookie();
    }

    public function awayCountProduct($id) {
        if ($this->basket[$id]['count'] != 1) {
            $this->basket[$id]['count']--;
            $this->setCookie();
        }
    }

    public function deleteProduct($id) {
        unset($this->basket[$id]);
        $this->setCookie();
    }

    public function deleteAllProduct() {
        $this->basket = [];
        $this->setCookie();
    }

    public function getBasket() {
        return $this->basket;
    }

    public function setCookie() {
        setcookie('Basket', json_encode($this->basket));
    }

}
$myBasket = Basket::getInstance();
//$myBasket->setProduct(1, 'Towar1', 2000);
//$myBasket->setProduct(2, 'Towar2', 2000);
//$myBasket->deleteProduct(1);
//$myBasket->awayCountProduct(1);
//$myBasket->deleteAllProduct();
$getBasket = $myBasket->getBasket();
print_r($getBasket);